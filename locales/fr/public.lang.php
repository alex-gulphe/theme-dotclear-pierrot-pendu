<?php
/*	Pour une lecture idéale !
	---------------------------------------------------------------------------------
	Adaptation en langue française du thème Pierrot pendu

	D’après :
	@package Dotclear
	@copyright Olivier Meunier & Association Dotclear
	@copyright GPL-2.0-only

	Version 2023-10-0.6 [active] : 2023-10-26
	Basé sur le jeu de templates « dotty »
	Auteur : L’atelier de Virginia Pearl par Alex Gulphe
	2023. Licence CC BY-NC-SA 4.0
	---------------------------------------------------------------------------------

	Pour la traduction personnalisée de l’interface publique.
	Source : https://tips.dotaddict.org/fiche/Ajouter-des-traductions-dans-les-thèmes
	--------------------------------------------------------------------------------- */

use Dotclear\Helper\L10n;

L10n::$locales['%d attachments'] = '%d&nbsp;annexes'; /* Mod */
L10n::$locales['%d comments'] = '%d&nbsp;commentaires'; /* Mod */
L10n::$locales['%d trackbacks'] = '%d rétroliens';
L10n::$locales['%s reactions'] = '%s réactions';
L10n::$locales['Access to content'] = 'Aller au contenu';
L10n::$locales['Add a comment'] = 'Ajouter un commentaire';
L10n::$locales['Add ping'] = 'Ajouter un rétrolien';
L10n::$locales['Additionnal style directives'] = 'Directives CSS supplémentaires';
L10n::$locales['All keywords'] = 'Tous les mots clés';
L10n::$locales['Archives'] = 'Collection Pierrot&nbsp;pendu'; /* Mod */
L10n::$locales['Attachments'] = 'En annexe'; /* Mod */
L10n::$locales['Best of me'] = 'À retenir';
L10n::$locales['Blog info'] = 'Informations';
L10n::$locales['Blog menu'] = 'Navigation&nbsp;: points de repère'; /* Mod */
L10n::$locales['By'] = 'Auteur, autrice&nbsp;:'; /* Mod */
L10n::$locales['By category'] = 'Par auteur, autrice et&nbsp;titre&nbsp;d’ouvrage'; /* Mod */
L10n::$locales['By date'] = 'Par date';
L10n::$locales['By tag'] = 'Par mot-clé';
L10n::$locales['Categories'] = 'Catégories';
L10n::$locales['Comment'] = 'Commentaire';
L10n::$locales['Comments'] = 'Commentaires';
L10n::$locales['Comments can be formatted using a simple wiki syntax.'] = 'Les commentaires peuvent être formatés en utilisant une syntaxe wiki simplifiée.';
L10n::$locales['Comments feed'] = 'Fil des commentaires';
L10n::$locales['Continue reading'] = 'Lire la suite…'; /* Mod */
L10n::$locales['Document not found'] = 'Le document demandé n’a pas été trouvé…'; /* Mod */
L10n::$locales['Email address'] = 'Adresse de courriel'; /* Mod */
L10n::$locales['Email address is not valid'] = 'Adresse de courriel incorrecte'; /* Mod */
L10n::$locales['Entries feed'] = 'Fil des billets';
L10n::$locales['Explore archives'] = 'de consulter son catalogue'; /* Mod */
L10n::$locales['Extra menu'] = 'Menu extra';
L10n::$locales['From'] = 'De';
L10n::$locales['FromDay'] = 'Du';
L10n::$locales['Go to homepage'] = 'd’accéder à sa page d’accueil'; /* Mod */
L10n::$locales['HTML code is displayed as text and web addresses are automatically converted.'] = 'Pour la saisie de votre commentaire, il vous est possible d’employer, sans&nbsp;distinction, les formats texte&nbsp;brut <span style="font-style:normal;">(plain&nbsp;text)</span>, HTML et&nbsp;Markdown.<br>Votre adresse de courriel ne sera pas rendue publique.'; /* Mod */
L10n::$locales['Home'] = 'Accueil';
L10n::$locales['In'] = 'Dans';
L10n::$locales['Languages'] = 'Langues';
L10n::$locales['Links'] = 'Liens';
L10n::$locales['Name or nickname'] = 'Nom ou pseudo';
L10n::$locales['Previous entry:'] = 'Texte précédent&nbsp;:'; /* Mod */
L10n::$locales['On'] = 'Le';
L10n::$locales['Page top'] = 'En haut'; /* Mod */
L10n::$locales['Pagination'] = 'Pagination';
L10n::$locales['Password needed'] = 'Mot de passe nécessaire';
L10n::$locales['Password:'] = 'Mot de passe&nbsp;:'; /* Mod */
L10n::$locales['Permalink'] = 'Permalien'; /* Mod */
L10n::$locales['Powered by %s'] = 'est propulsé par&nbsp;%s'; /* Mod */
L10n::$locales['Preview'] = 'Prévisualiser';
L10n::$locales['Next entry:'] = 'Texte suivant&nbsp;:'; /* Mod */
L10n::$locales['RSS feed is a free blog summary. It provides content (either posts or comments) or summaries of content, together with links to the full versions, and other metadata. The last published items may then be read by your favorite RSS <a href="http://en.wikipedia.org/wiki/Aggregator">aggregator</a>.'] = 'Un fil RSS recueille les informations de mise à jour d\'un site. Il fournit le contenu des billets ou des commentaires ou un extrait de ceux-ci, ainsi qu\'un lien vers les versions complètes et quelques autres informations. Ce fil a pour vocation d\'être lu par un <a href="http://fr.wikipedia.org/wiki/Agr%C3%A9gateur">agrégateur</a> RSS.';
L10n::$locales['Read'] = 'Lire';
L10n::$locales['Remember me on this blog'] = 'Se souvenir de moi sur ce site.'; /* Mod */
L10n::$locales['Required field'] = 'Champs obligatoire';
L10n::$locales['Search'] = 'Recherche';
L10n::$locales['Send'] = 'Envoyer';
L10n::$locales['Simply copy the following URL into your aggregator:'] = 'Copier simplement l’adresse suivante dans votre agrégateur&nbsp;:'; /* Mod */
L10n::$locales['Subcategories'] = 'Sous-catégories';
L10n::$locales['Subscribe'] = 'S\'abonner';
L10n::$locales['Subscribe to'] = 'S\'abonner à';
L10n::$locales['Suggestions:'] = 'Pierrot pendu vous suggère&nbsp;:'; /* Mod */
L10n::$locales['The document you are looking for does not exist.'] = 'Le document que vous cherchez n\'existe pas.';
L10n::$locales['The list of recents posts is available'] = 'La liste des publications récentes est disponible';
L10n::$locales['They posted on the same topic'] = 'La discussion continue ailleurs';
L10n::$locales['This blog\'s comments Atom feed'] = 'Fil Atom des commentaires de ce site'; /* Mod */
L10n::$locales['This category\'s comments Atom feed'] = 'Fil Atom des commentaires de cette catégorie';
L10n::$locales['This category\'s entries Atom feed'] = 'Fil Atom des billets de cette catégorie';
L10n::$locales['This post\'s comments Atom feed'] = 'Fil Atom des commentaires de ce billet';
L10n::$locales['This post\'s comments feed'] = 'Fil des commentaires de ce billet';
L10n::$locales['To content'] = 'Aller au contenu';
L10n::$locales['To menu'] = 'Aller au menu';
L10n::$locales['To search'] = 'Aller à la recherche';
L10n::$locales['Trackback URL'] = 'URL de rétrolien';
L10n::$locales['URL you\'ve tried has typos, or the page has been deleted or moved.'] = 'L’URL saisie est incorrecte, ou la page a été supprimée ou déplacée.'; /* Mod */
L10n::$locales['Use search form'] = 'd’effectuer une recherche dans ses pages'; /* Mod */
L10n::$locales['Website'] = 'Site web';
L10n::$locales['What is an RSS feed?'] = 'Qu’est ce qu’un fil RSS&#8239;?'; /* Mod */
L10n::$locales['You must give a password to access this area.'] = 'Vous devez indiquer un mot de passe pour accéder à cette partie.';
L10n::$locales['You must provide a comment'] = 'Vous devez écrire un commentaire.'; /* Mod */
L10n::$locales['You must provide a valid email address.'] = 'Vous devez indiquer une adresse de courriel valide.'; /* Mod */
L10n::$locales['You must provide an author name'] = 'Vous devez indiquer un nom.'; /* Mod */
L10n::$locales['Your comment'] = 'Votre commentaire';
L10n::$locales['Your comment has been published.'] = 'Votre commentaire a été publié.';
L10n::$locales['Your comment has been submitted and will be reviewed for publication.'] = 'Votre commentaire est enregistré. Il sera publié après&nbsp;validation.'; /* Mod */
L10n::$locales['Your search for <em>%1$s</em> returned <strong>%2$s</strong> result.'] = 'Votre recherche pour «&nbsp;%1$s&nbsp;» a donné <strong>%2$s&nbsp;résultat</strong>.'; /* Mod */
L10n::$locales['Your search for <em>%1$s</em> returned <strong>%2$s</strong> results.'] = 'Votre recherche pour «&nbsp;%1$s&nbsp;» a donné <strong>%2$s&nbsp;résultats</strong>.'; /* Mod */
L10n::$locales['Your search for <em>%1$s</em> returned no result.'] = 'Votre recherche pour «&nbsp;%1$s&nbsp;» n’a donné <strong>aucun&nbsp;résultat</strong>.'; /* Mod */
L10n::$locales['by'] = 'par';
L10n::$locales['here'] = 'ici';
L10n::$locales['in'] = 'dans';
L10n::$locales['previous entries'] = 'textes précédents'; /* Mod */
L10n::$locales['no attachments'] = 'aucune annexe';
L10n::$locales['no comments'] = 'aucun commentaire';
L10n::$locales['no reactions'] = 'pas de réactions';
L10n::$locales['no trackbacks'] = 'aucun rétrolien';
L10n::$locales['of'] = 'de';
L10n::$locales['on'] = 'le';
L10n::$locales['one attachment'] = 'une annexe';
L10n::$locales['one comment'] = 'un commentaire';
L10n::$locales['one reaction'] = 'une réaction';
L10n::$locales['one trackback'] = 'un rétrolien';
L10n::$locales['optional'] = 'facultatif';
L10n::$locales['page'] = 'page';
L10n::$locales['preview'] = 'prévisualiser';
L10n::$locales['next entries'] = 'textes suivants'; /* Mod */
L10n::$locales['reactions'] = 'réactions';
L10n::$locales['send'] = 'envoyer';
L10n::$locales['toDay'] = 'au';

/*	Pour une lecture idéale ! Ajouts
	--------------------------------------------------------------------------------- */
L10n::$locales['new window'] = 'site distant';
L10n::$locales['Pages'] = 'Nos belles pages';

/* Incorrect pour la validation du champ e-mail du formulaire présent dans _entry-feedback.html
	--------------------------------------------------------------------------------- */
L10n::$locales['Email address is not valid.'] = 'Merci de saisir une adresse de courriel valide.';
L10n::$locales['You must provide a valid email address.'] = 'Merci de saisir une adresse de courriel valide.';
