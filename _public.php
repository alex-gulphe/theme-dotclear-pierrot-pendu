<?php
/*	Pour une lecture idéale !
	---------------------------------------------------------------------------------
	Adaptation en langue française du thème Pierrot pendu

	D’après :
	@package Dotclear
	@copyright Olivier Meunier & Association Dotclear
	@copyright GPL-2.0-only

	Version 2023-10-0.6 [active] : 2023-10-26
	Basé sur le jeu de templates « dotty »
	Auteur : L’atelier de Virginia Pearl par Alex Gulphe
	2023. Licence CC BY-NC-SA 4.0
	---------------------------------------------------------------------------------

	Pour la traduction personnalisée de l’interface publique.
	Source : https://tips.dotaddict.org/fiche/Ajouter-des-traductions-dans-les-thèmes
	--------------------------------------------------------------------------------- */

#	Empêcher l’exécution du fichier en dehors de Dotclear

if (!defined('DC_RC_PATH')) {return;}

l10n::set(dirname(__FILE__).'/locales/'.$_lang.'/public');
?>