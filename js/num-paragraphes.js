/*  Insertion d’identifiants numériques incrémentés afin de permettre la navigation dans le texte.
    L’index débutant à "0" par défaut, le titre de niveau 2 est inclus afin de permettre la coïncidence entre la numérotation en marge des paragraphes et les valeurs distribuées aux identifiants.
    Cette méthode n’est pas exempte de défauts, mais elle semble remplir son rôle, sous réserve de la prise en charge de JavaScript par le navigateur ! */

//  Prise en compte du titre de niveau 2 et des paragraphes.
document.querySelectorAll('h2.post-title, .post-content p').forEach(function (refId, index) {
    refId.insertAdjacentHTML ('afterBegin', '<a id="' + index + '" class="refID"></a>');
});
//  Prise en compte des titres de niveau 3, 4, 5, 6 avec un identifiant distinct.
document.querySelectorAll('.post-content > h3, h4, h5, h6').forEach(function (refId, index) {
    refId.insertAdjacentHTML ('afterBegin', '<a id="tt' + index + '" class="refID"></a>');
});