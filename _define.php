<?php
/**
 * @brief Pierrot pendu, a theme for Dotclear 2
 *
 * @package Dotclear
 * @subpackage Themes
 *
 * @copyright Olivier Meunier & Association Dotclear
 * @copyright GPL-2.0-only
 * @copyright L’atelier de Virginia Pearl par Alex Gulphe
 * 2023. Licence CC BY-NC-SA 4.0
 *
 * --- Thème installé le 2023-11-06 ---
 * --- Thème déposé (GitLab) le 2024-01-24 ---
 * --- Dépôt public disponible à l’adresse https://gitlab.com/alex-gulphe/theme-dotclear-pierrot-pendu ---
 */
$this->registerModule(
    'Pierrot pendu [active]',                                     // Name
    'A theme based on typographic’s rules to publish old books',  // Description
    'L’atelier de Virginia Pearl par Alex Gulphe',                // Author
    '2023-10-0.6',                                                // Version
    [                                                             // Properties
        'type'   => 'theme',
        'tplset' => 'dotty',
    ]
);
