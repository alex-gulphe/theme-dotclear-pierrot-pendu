# CC BY-NC-SA 4.0 International
Attribution – Pas d’utilisation commerciale – Partage dans les mêmes conditions 4.0 International \
[Source](https://creativecommons.org/licenses/by-nc-sa/4.0/).

Vous êtes autorisé à :
- Partager —&#8239;copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
- Adapter —&#8239;remixer, transformer et créer à partir du matériel
- L’offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.

Selon les conditions suivantes :
- Attribution —&#8239;Vous devez créditer l’œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l’œuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l’offrant vous soutient ou soutient la façon dont vous avez utilisé son œuvre.
- Pas d’utilisation commerciale —&#8239;Vous n’êtes pas autorisé à faire un usage commercial de cette œuvre, tout ou partie du matériel la composant.
- Partage dans les mêmes conditions —&#8239;Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l’œuvre originale, vous devez diffuser l’œuvre modifiée dans les même conditions, c’est à dire avec la même licence avec laquelle l’œuvre originale a été diffusée.
- Pas de restrictions complémentaires —&#8239;Vous n’êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l’œuvre dans les conditions décrites par la licence.

Notes :
- Vous n’êtes pas dans l’obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l’utilisation que vous souhaitez faire est couverte par une exception.
- Aucune garantie n’est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme les droits moraux, le droit des données personnelles et le droit à l’image sont susceptibles de limiter votre utilisation.

***

Pour plus d’information, [consulter le code juridique de la licence CC BY-NC-SA 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.fr).
