<?php
/**
 * @brief Pierrot pendu, a theme for Dotclear 2
 *
 * @package Dotclear
 * @subpackage Themes
 *
 * @copyright Olivier Meunier & Association Dotclear
 * @copyright GPL-2.0-only
 *
 * @since 2.27
 *

 * @copyright L’atelier de Virginia Pearl par Alex Gulphe
 * 2023. Licence CC BY-NC-SA 4.0
 */
declare(strict_types=1);

namespace Dotclear\Theme\pierrot-pendu-dev;

use Dotclear\Module\MyTheme;

class My extends MyTheme
{
}
