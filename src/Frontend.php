<?php
/**
 * @brief Pierrot pendu, a theme for Dotclear 2
 *
 * @package Dotclear
 * @subpackage Themes
 *
 * @copyright Olivier Meunier & Association Dotclear
 * @copyright GPL-2.0-only
 * @copyright L’atelier de Virginia Pearl par Alex Gulphe
 * 2023. Licence CC BY-NC-SA 4.0
 */

namespace Dotclear\Theme\pierrot-pendu-dev;

use dcCore;
use Dotclear\Core\Process;

class Frontend extends Process
{
    public static function init(): bool
    {
        return self::status(My::checkContext(My::FRONTEND));
    }

    public static function process(): bool
    {
        if (self::status()) {
            dcCore::app()->addBehavior('publicHeadContent', function () {
                echo 
                '<link rel="stylesheet" type="text/css" href="' . 
                dcCore::app()->blog->settings->system->public_url . 
                '/custom_style.css" media="screen">' . "\n";
            });
        }

        return self::status();
    }
}
